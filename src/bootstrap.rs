#![allow(dead_code)]

mod cli;
use {
    clap::{Arg, Command},
    cli::opts::{build, build_cli},
    package_bootstrap::Bootstrap,
    std::{env, error::Error, fs, os::unix::fs::DirBuilderExt, path::PathBuf, process},
};

fn copy_data(outdir: &str) -> Result<(), Box<dyn Error>> {
    println!("Copying data files:");
    let appdir: PathBuf = [outdir, "share", "applications"].iter().collect();
    let mut dirbuilder = fs::DirBuilder::new();
    dirbuilder.recursive(true).mode(0o0755);
    if !appdir.exists() {
        dirbuilder.create(&appdir)?;
    }
    let mut outfile = appdir;
    outfile.push("gfret.desktop");
    let infile: PathBuf = ["data", "gfret.desktop"].iter().collect();
    fs::copy(&infile, &outfile)?;
    println!("    {} -> {}", infile.display(), outfile.display());
    let schemadir: PathBuf = [outdir, "share", "glib-2.0", "schemas"].iter().collect();
    if !schemadir.exists() {
        dirbuilder.create(&schemadir)?;
    }
    let mut outfile = schemadir.clone();
    outfile.push("org.hitchhiker_linux.gfret.gschema.xml");
    let infile: PathBuf = ["data", "org.hitchhiker_linux.gfret.gschema.xml"]
        .iter()
        .collect();
    fs::copy(&infile, &outfile)?;
    println!("    {} -> {}", infile.display(), outfile.display());
    let basedir = env::current_dir()?;
    if !schemadir.starts_with(&basedir) && schemadir.starts_with("/") {
        println!("Compiling gschemas");
        let _status = process::Command::new("glib-compile-schemas")
            .arg(schemadir.to_str().unwrap())
            .status()?;
    }
    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    let matches = Command::new("bootstrap")
        .about("install the software")
        .author("Nathan Fisher")
        .version(env!("CARGO_PKG_VERSION"))
        .args([
            Arg::new("target-dir")
                .help("the directory where the 'gfret' binary is located")
                .short('t')
                .long("target-dir")
                .num_args(1),
            Arg::new("output")
                .help("the output directory for the installation")
                .required(true)
                .num_args(1),
        ])
        .get_matches();
    let outdir = matches.get_one::<String>("output").unwrap().to_string();
    let out = PathBuf::from(&outdir);
    let target_dir = matches
        .get_one::<String>("target-dir")
        .map(|x| x.to_string());
    Bootstrap::new("gfret", build(), &out).install(target_dir, 1)?;
    Bootstrap::new("gfret-cli", build_cli(), &out).manpage(1)?;
    copy_data(&outdir)?;
    Ok(())
}
